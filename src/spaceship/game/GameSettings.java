package spaceship.game;

public class GameSettings {
	private int numAsteroids, numStars, numEnemies;
	public GameSettings(int numAsteroids, int numStars,int numEnemies)
	{
		this.setNumAsteroids(numAsteroids);
		this.setNumEnemies(numEnemies);
		this.setNumStars(numStars);
	}
	
	public int getNumAsteroids() {
		return numAsteroids;
	}
	public void setNumAsteroids(int numAsteroids) {
		this.numAsteroids = numAsteroids;
	}

	public int getNumEnemies() {
		return numEnemies;
	}

	public void setNumEnemies(int numEnemies) {
		this.numEnemies = numEnemies;
	}

	public int getNumStars() {
		return numStars;
	}

	public void setNumStars(int numStars) {
		this.numStars = numStars;
	}
	
	
}
