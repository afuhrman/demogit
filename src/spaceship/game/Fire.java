package spaceship.game;

import java.awt.Color;
import java.awt.Graphics2D;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedList;

import spaceship.Main;

public class Fire {
	private LinkedList<Shot> shots = new LinkedList<Shot>();

	private Main mf;
	private Ship ship = null;
	private Enemy[] enemies = null;
	private int pain;
	private Color color = new Color(0,0,255);

	public Fire(Main mf, int pain) {
		this.mf = mf;
		this.pain = pain;
	}

	public void setColor(Color color)
	{
		this.color = color;
	}
	
	public void setEnemy(Ship ship) {
		enemies = null;
		this.ship = ship;
	}

	public void setEnemy(Enemy[] enemies) {
		ship = null;
		this.enemies = enemies;
	}

	public void addShot(float x, float y) {
		if ((enemies != null || ship != null)) {
				shots.add(new Shot(x, y, 1, 10,color));
		}
	}

	public void draw(Graphics2D g2) {
		for (int i = 0; i < shots.size(); i++) {
			shots.get(i).draw(g2);
		}
	}

	public void refresh() {
		Iterator<Shot> iterator = shots.iterator();

		while (iterator.hasNext()) {
			Shot shot = iterator.next();
			if (isOutsideWindow(shot)) {
				iterator.remove();
			} else {
				if (ship == null) {
					for (int j = 0; j < enemies.length; j++) {
						if (hasBeenHit(shot, enemies[j])) {
							iterator.remove();
						}
					}
					shot.moveRight();
				} else if (enemies == null) {
					if (hasBeenHit(shot, ship)) {
						iterator.remove();
					}
					shot.moveLeft();
				}
			}
		}
	}
	
	private boolean isOutsideWindow(Shot shot) {
		return shot.getX() + shot.getSize() < 0 || shot.getX() - shot.getSize() > mf.getWidth();
	}

	private boolean hasBeenHit(Shot shot, MovingObject object) {
		if (Math.sqrt(Math.pow(shot.getX() - object.getX(), 2) + Math.pow(shot.getY() - object.getY(), 2)) < shot.getSize() / 2 + object.getSize() / 2) {
			object.reduceLife(pain);
			return true;
		}
		return false;
	}
}
