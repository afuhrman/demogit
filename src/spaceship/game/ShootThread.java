package spaceship.game;

public class ShootThread extends Thread {
	Enemy e;
	public boolean fire;
	public ShootThread(Enemy e)
	{
		this.e = e;
		fire = false;
		start();
	}
	
	public void startFire()
	{
		fire = true;
	}
	
	public void stopFire()
	{
		fire = false;
	}
	
	public boolean fire(){
		return fire;
	}
	public void run()
	{
		while(true)
		{
			if(fire)
				e.getFire().addShot(e.getX() -e.getSize(),
						e.getY());
			
		}
	}
}
