package spaceship.game;

import java.awt.Graphics2D;

import spaceship.Main;

public class StarField {
	private int frameWidth, frameHeight;
	private Star[] stars;
	private Main mf;
	public StarField(Main mf) {
		this.mf = mf;
		this.frameWidth = mf.getWidth();
		this.frameHeight = mf.getHeight();
		init();
	}

	public void init() {
		stars = new Star[mf.getGameSettings().getNumStars()];
		for (int i = 0; i < stars.length; i++) {
			stars[i] = (new Star((float) (Math.random() * frameWidth),
					(float) (Math.random() * frameHeight),
					(float) (Math.random() * 0.05) + 0.005f,
					(float) Math.random() * 5 + 2));
		}
	}

	public void draw(Graphics2D g2) {
		for (int i = 0; i < stars.length; i++)
			stars[i].draw(g2);
	}

	public void refresh() {
		for (int i = 0; i < stars.length; i++) {
			if (stars[i].getX() < 0 - stars[i].getSize()) {
				stars[i] = new Star((float) (frameWidth + 20),
						(float) (Math.random() * frameHeight),
						(float) (Math.random() * 0.05) + 0.005f,
						(float) Math.random() * 5 + 2);

			}
			stars[i].moveLeft();
		}
	}

	public Star[] getAsteroids() {
		return stars;
	}

	public void setAsteroids(Star[] stars) {
		this.stars = stars;
	}

}
