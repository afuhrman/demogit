package spaceship.game;

import java.awt.Graphics2D;

import spaceship.Main;

public class AsteroidField {
	private int frameWidth, frameHeight;
	private Asteroid[] asteroids;
	private Main mf;
	public AsteroidField(Main mf) {
		this.mf = mf;
		this.frameWidth = mf.getWidth();
		this.frameHeight = mf.getHeight();
		init();
	}

	public void init() {
		asteroids = new Asteroid[mf.getGameSettings().getNumAsteroids()];
		for (int i = 0; i < asteroids.length; i++) {
			asteroids[i] = (new Asteroid(
					(float) (frameWidth + 20 + Math.random() * 1000),
					(float) (Math.random() * frameHeight),
					(float) (Math.random() * 0.15) + 0.01f, (float)(Math.random() * 70 + 30)));
		}
	}

	public void draw(Graphics2D g2) {
		for (int i = 0; i < asteroids.length; i++)
			asteroids[i].draw(g2);
	}

	public void refresh() {
		for (int i = 0; i < asteroids.length; i++) {
			if (asteroids[i].getX() < 0 - asteroids[i].getSize()) {
				asteroids[i] = new Asteroid(
						(float) (frameWidth + 20 + Math.random() * 1000),
						(float) (Math.random() * frameHeight),
						(float) (Math.random() * 0.15) + 0.01f, (float)(Math.random() * 70 + 30));

			}
			asteroids[i].moveLeft();
		}
	}

	public Asteroid[] getAsteroids() {
		return asteroids;
	}

	public void setAsteroids(Asteroid[] asteroids) {
		this.asteroids = asteroids;
	}

}
