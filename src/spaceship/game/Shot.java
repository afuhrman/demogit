package spaceship.game;

import java.awt.Color;
import java.awt.Graphics2D;

public class Shot extends MovingObject {

	Color color = new Color(0,0,255);
	public Shot(float x, float y, float v, float size) {
		super(x, y, v, size);
	}
	public Shot(float x, float y, float v, float size, Color color) {
		super(x, y, v, size);
		this.color = color;
	}
	
	public void setColor(Color color)
	{
		this.color = color;
	}
	
	public void draw(Graphics2D g2)
	{
		g2.setColor(color);
		g2.fillOval((int)((int) (getX() - (getSize() / 2))), (int) (getY() - getSize() / 2), (int) getSize(),
				(int) getSize());
	}

}
