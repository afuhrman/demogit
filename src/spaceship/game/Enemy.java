package spaceship.game;

import java.awt.Color;
import java.awt.Graphics2D;

public class Enemy extends MovingObject {
	private Ship ship;
	private Color color;
	Fire fire;
	Timer timer;
	private int life = 1;

	public Timer getTimer()
	{
		return timer;
	}
	
	public Enemy(float x, float y, float v, float size, Ship ship) {
		super(x, y, v, size);
		this.ship = ship;
		timer = new Timer(100);
		color = new Color(255, 0, 0);
	}

	public void setFire(Fire fire) {
		this.fire = fire;
	}

	public Fire getFire() {
		return fire;
	}

	public void reduceLife(int pain) {
		life -= pain;
	}

	public boolean isDead() {
		return life <= 0;
	}

	public void draw(Graphics2D g2) {
		g2.setColor(color);
		int[] dx = { (int) (getX() - getSize()), (int) getX(), (int) getX() };
		int[] dy = { (int) getY(), (int) (getY() - getSize() / 2),
				(int) (getY() + getSize() / 2) };
		g2.fillPolygon(dx, dy, 3);
	}

	public void refresh() {

	}

}
