package spaceship.game;

import java.awt.Color;
import java.awt.Graphics2D;

public class Ship extends MovingObject {
	private Color color = new Color(0, 255, 0);
	private Fire fire;
	private int life = 100;
	public Ship(float x, float y, float v, float size) {
		super(x,y,v,size);
	}
	
	public void setFire(Fire fire)
	{
		this.fire = fire;
	}
	
	public Fire getFire()
	{
		return fire;
	}

	public void reduceLife(int pain)
	{
		life -= pain;
	}
	
	public int getLife()
	{
		return life;
	}
	
	public boolean isDead()
	{
		return life <= 0;
	}
	
	private float topBorder(float u)
	{
		return (1/2)*(u-getX()) + getY() - (getSize()/2);
	}
	
	private float bottomBorder(float u)
	{
		return -(1/2)*(u-getX()) + getY() + (getSize()/2);
	}
	
	public boolean contactRoundObject(float tu, float tv, float radius)
	{
		if(tu >= getX() && tu <= getX() + getSize())
		{
			if(tv >= topBorder(tu) && tv <= bottomBorder(tu) )
			{
				return true;
			}
		}
		return false;
	}
	
	public Color getColor() {
		return color;
	}

	public void setColor(Color color) {
		this.color = color;
	}

	public void shoot()
	{
		fire.addShot(getX() + getSize(), getY());
	}
	
	public void draw(Graphics2D g2) {
		g2.setColor(color);
		int[] xC = { (int) getX(), (int) getX(), (int) (getX() + getSize()) };
		int[] yC = { (int) (getY() - (getSize() / 2)), (int) (getY() + (getSize() / 2)), (int) getY()};
		g2.fillPolygon(xC, yC, 3);
	}
}
