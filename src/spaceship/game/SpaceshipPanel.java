package spaceship.game;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.RenderingHints;
import java.awt.event.KeyEvent;

import spaceship.Main;
import spaceship.SuperPanel;
import spaceship.menus.MainMenuPanel;
import javax.swing.JLabel;
import java.awt.Font;
import javax.swing.SwingConstants;

public class SpaceshipPanel extends SuperPanel {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private Main mf;
	private GameSettings gs;
	private Ship ship;
	private StarField stars;
	private AsteroidField asteroids;
	private EnemyField enemies;
	private GameThread g;
	private boolean shipShot;
	private JLabel life;

	/**
	 * Create the panel.
	 */
	@SuppressWarnings("deprecation")
	public SpaceshipPanel(Main mf) {
		super();

		this.mf = mf;
		this.gs = mf.getGameSettings();
		setBounds(mf.bounds());
		setBackground(new Color(0, 0, 0));
		ship = new Ship(100, getHeight() / 2, 10, 50);
		stars = new StarField(mf);
		asteroids = new AsteroidField(mf);
		enemies = new EnemyField(mf, ship);

		ship.setFire(new Fire(mf, 1));
		ship.getFire().setEnemy(enemies.getEnemies());

		this.g = new GameThread(this);
		setLayout(null);

		JLabel lblShipLife = new JLabel("New label");
		lblShipLife.setHorizontalAlignment(SwingConstants.CENTER);
		lblShipLife.setFont(new Font("Tahoma", Font.PLAIN, 30));
		lblShipLife.setBounds(30, 16, 100, 50);
		lblShipLife.setForeground(Color.YELLOW);
		add(lblShipLife);
		life = lblShipLife;
	}

	public void paintComponent(Graphics g) {
		super.paintComponent(g);
		Graphics2D g2 = (Graphics2D) g;
		g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING,
				RenderingHints.VALUE_ANTIALIAS_ON);
		g2.setRenderingHint(RenderingHints.KEY_RENDERING,
				RenderingHints.VALUE_RENDER_QUALITY);
		stars.draw(g2);
		ship.draw(g2);
		asteroids.draw(g2);
		enemies.draw(g2);
		ship.getFire().draw(g2);
	}

	private float distanceShipToAsteroid(Asteroid a) {
		// untere Grenze
		// Schnittpunkt der Normalen zur G durch M mit G
		float fX = (2.0f / 5.0f)
				* ((ship.getX() / 2) + ship.getY() + (ship.getSize() / 2)
						- a.getY() + 2 * a.getX());
		float distanceDown;
		if (fX <= ship.getX() + ship.getSize() && fX >= ship.getX()) {
			float fY = 2 * fX + a.getY() - 2 * a.getX();
			float dX = fX - a.getX();
			float dY = fY - a.getY();
			distanceDown = (float) Math.sqrt(dX * dX + dY * dY) - a.getSize()
					/ 2;

			/*
			 * Graphics2D g2 = (Graphics2D)super.getGraphics(); g2.setColor(new
			 * Color(255,0,0)); g2.setPaintMode();
			 * g2.drawLine((int)a.getX(),(int)a.getY(),(int)fX,(int)fY);
			 */

		} else {
			distanceDown = (float) Math.sqrt((ship.getY() - a.getY())
					* (ship.getY() - a.getY())
					+ (ship.getX() + ship.getSize() - a.getX())
					* (ship.getX() + ship.getSize() - a.getX()))
					- a.getSize() / 2;
		}
		float distanceUp;
		// obere Grenze
		fX = (2.0f / 5.0f)
				* ((ship.getX() / 2) - ship.getY() + (ship.getSize() / 2)
						+ a.getY() + 2 * a.getX());
		if (fX <= ship.getX() + ship.getSize() && fX >= ship.getX()) {
			float fY = -2 * fX + a.getY() + 2 * a.getX();
			float dX = fX - a.getX();
			float dY = fY - a.getY();
			distanceUp = (float) Math.sqrt(dX * dX + dY * dY) - a.getSize() / 2;

			/*
			 * Graphics2D g2 = (Graphics2D)super.getGraphics(); g2.setColor(new
			 * Color(255,0,0)); g2.setPaintMode();
			 * g2.drawLine((int)a.getX(),(int)a.getY(),(int)fX,(int)fY);
			 */

		} else {
			distanceUp = (float) Math.sqrt((ship.getY() - a.getY())
					* (ship.getY() - a.getY())
					+ (ship.getX() + ship.getSize() - a.getX())
					* (ship.getX() + ship.getSize() - a.getX()))
					- a.getSize() / 2;
		}
		if (distanceDown < distanceUp)
			return distanceDown;
		else
			return distanceUp;
	}

	public boolean collision() {
		Asteroid temp;
		for (int i = 0; i < asteroids.getAsteroids().length; i++) {
			temp = asteroids.getAsteroids()[i];
			if (distanceShipToAsteroid(temp) <= 0)
				return true;

		}
		return false;
	}

	@SuppressWarnings("deprecation")
	public void refresh() {
		if (life != null)
			life.setText("" + ship.getLife());
		if (shipShot) {
			ship.shoot();
			shipShot = false;
		}
		stars.refresh();
		asteroids.refresh();
		enemies.refresh();
		ship.getFire().refresh();
		if (collision()) {

			mf.initMenu(new MainMenuPanel(mf));
			g.stop();
		}
		repaint();
	}

	@SuppressWarnings("static-access")
	public void keyPressedAction(KeyEvent e) {
		if (e.getKeyCode() == e.VK_RIGHT
				&& ship.getX() < getWidth() - ship.getSize()) {
			ship.moveRight();
		}
		if (e.getKeyCode() == e.VK_LEFT && ship.getX() > 0) {
			ship.moveLeft();
		}
		if (e.getKeyCode() == e.VK_UP && ship.getY() > 0 + ship.getSize() / 2) {
			ship.moveUp();
		}
		if (e.getKeyCode() == e.VK_DOWN
				&& ship.getY() < getHeight() - ship.getSize() / 2) {
			ship.moveDown();
		}

	}

	@Override
	public void keyTypedAction(KeyEvent e) {
		// TODO Auto-generated method stub

	}

	@Override
	public void keyReleasedAction(KeyEvent e) {
		if (e.getKeyCode() == e.VK_CONTROL) {
			shipShot = true;
		}

	}

	public Main getMf() {
		return mf;
	}

	public void setMf(Main mf) {
		this.mf = mf;
	}

	public GameSettings getGs() {
		return gs;
	}

	public void setGs(GameSettings gs) {
		this.gs = gs;
	}

	public Ship getShip() {
		return ship;
	}

	public void setShip(Ship ship) {
		this.ship = ship;
	}

	public StarField getStars() {
		return stars;
	}

	public void setStars(StarField stars) {
		this.stars = stars;
	}

	public AsteroidField getAsteroids() {
		return asteroids;
	}

	public void setAsteroids(AsteroidField asteroids) {
		this.asteroids = asteroids;
	}

	public GameThread getG() {
		return g;
	}

	public void setG(GameThread g) {
		this.g = g;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}
}
