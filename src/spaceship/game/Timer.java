package spaceship.game;

public class Timer {
	private int startTime, interval;
	private boolean over;

	// Millisekunden
	public Timer(int interval) {
		this.interval = interval;
		startTime = (int) (System.nanoTime() / 1000000);
	}

	public boolean timeOver() {
		if (over) {
			over = false;
			return true;
		} else
			return false;
	}

	public void refresh() {
		int recentTime = (int) (System.nanoTime() / 1000000) - startTime;
		if (recentTime > interval) {
			startTime = (int) (System.nanoTime() / 1000000);
			over = true;
		}
	}

}
