package spaceship.game;

public class GameThread extends Thread {
	SpaceshipPanel s;

	public GameThread(SpaceshipPanel s) {
		this.s = s;
		this.start();
	}

	public void run() {
		while (true) {
			try {
				sleep(1);
			} catch (InterruptedException e) {
			}
			s.refresh();
		}
	}
}
