package spaceship.game;

import java.awt.Color;
import java.awt.Graphics2D;

public class Asteroid  extends MovingObject{
	private Color color;

	public Asteroid(float x, float y, float v, float size) {
		super(x,y,v,size);
		int c = (int) (50 + Math.random() * 70);
		color = new Color(c, c, c);

	}
	
	public Color getColor() {
		return color;
	}

	public void setColor(Color color) {
		this.color = color;
	}

	public void draw(Graphics2D g2) {
		g2.setColor(color);
		g2.fillOval((int) (getX() - (getSize() / 2)), (int) (getY() - getSize() / 2), (int) getSize(),
				(int) getSize());
	}


}
