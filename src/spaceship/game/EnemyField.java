package spaceship.game;

import java.awt.Color;
import java.awt.Graphics2D;

import spaceship.Main;

public class EnemyField {
	Enemy[] enemies = new Enemy[0];
	Main mf;
	Ship ship;

	public EnemyField(Main mf, Ship ship) {
		this.mf = mf;
		this.ship = ship;
		init();
	}

	public void init() {
		enemies = new Enemy[mf.getGameSettings().getNumEnemies()];
		for (int i = 0; i < enemies.length; i++) {
			enemies[i] = new Enemy((float) (Math.random() * mf.getWidth()
					+ mf.getWidth() + 20),
					(float) (Math.random() * mf.getHeight()),
					(float) (Math.random() * 0.3f) + 0.02f, 50, ship);

			enemies[i].setFire(new Fire(mf, 1));
			enemies[i].getFire().setEnemy(ship);
			enemies[i].getFire().setColor(new Color(255,255,0));
		}
	}

	public void draw(Graphics2D g2) {
		for (int i = 0; i < enemies.length; i++) {
			enemies[i].draw(g2);
			enemies[i].getFire().draw(g2);
		}
	}

	public void refresh() {
		for (int i = 0; i < enemies.length; i++) {
			if (enemies[i].getX() < 0 - enemies[i].getSize()
					|| enemies[i].isDead()) {
				{
					enemies[i] = new Enemy((float) (Math.random()
							* mf.getWidth() + mf.getWidth() + 20),
							(float) (Math.random() * mf.getHeight()),
							(float) (Math.random() * 0.3f) + 0.02f, 50, ship);
					enemies[i].setFire(new Fire(mf, 1));
					enemies[i].getFire().setEnemy(ship);
					enemies[i].getFire().setColor(new Color(255,255,0));
				}
			}
			if (enemies[i].getX() > ship.getX()
					&& enemies[i].getY() < mf.getWidth()
					&& Math.abs(enemies[i].getY() - ship.getY()) < ship
							.getSize() / 2) {
				enemies[i].getTimer().refresh();
				if(enemies[i].getTimer().timeOver())
					enemies[i].getFire().addShot(enemies[i].getX()-enemies[i].getSize(), enemies[i].getY());
			}
			

			enemies[i].getFire().refresh();
			enemies[i].moveLeft();
		}
	}

	public Enemy[] getEnemies() {
		return enemies;
	}

	public void setEnemies(Enemy[] enemies) {
		this.enemies = enemies;
	}

}
