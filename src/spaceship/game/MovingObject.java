package spaceship.game;

public class MovingObject {
	private float x, y, v, size;
	
	public MovingObject(float x, float y, float v, float size) {
		this.x = x;
		this.y = y;
		this.size = size;
		this.v = v;
	}
	
	public float getX() {
		return x;
	}

	public void setX(float x) {
		this.x = x;
	}

	public float getY() {
		return y;
	}

	public void setY(float y) {
		this.y = y;
	}

	public float getV() {
		return v;
	}

	public void setV(float v) {
		this.v = v;
	}

	public float getSize() {
		return size;
	}

	public void setSize(float size) {
		this.size = size;
	}
	

	public void moveRight() {
		x += v;
	}

	public void moveLeft() {
		x -= v;
	}

	public void moveUp() {
		y -= v;
	}

	public void moveDown() {
		y += v;
	}

	public void reduceLife(int pain)
	{
		// TODO Think about making this method abstract.
	}
}
