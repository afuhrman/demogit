package spaceship;

import java.awt.EventQueue;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

import javax.swing.JFrame;

import spaceship.game.GameSettings;
import spaceship.game.SpaceshipPanel;
import spaceship.menus.*;

@SuppressWarnings("serial")
public class Main extends JFrame {

	private SuperPanel contentPane = new MainMenuPanel(this);
	private GameSettings gs;
	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Main frame = new Main();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public Main() {
		setTitle("Spaceship Game");
		setStandardGameSettings();
		setResizable(false);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(0, 0, 1366, 768);
		setContentPane(contentPane);
		setFocusable(true);
		addKeyListener(new KeyListener() {

			@Override
			public void keyTyped(KeyEvent e) {
				contentPane.keyTypedAction(e);
			}

			@Override
			public void keyPressed(KeyEvent e) {
				if (e.getKeyCode() == KeyEvent.VK_ESCAPE)
					System.exit(0);
				contentPane.keyPressedAction(e);
				contentPane.keyReleasedAction(e);
			}

			@Override
			public void keyReleased(KeyEvent e) {
				contentPane.keyReleasedAction(e);
			}

		});

	}

	public void initGame(SpaceshipPanel p) {
		contentPane = p;
		setContentPane(contentPane);
		contentPane.revalidate();
		contentPane.repaint();
	}

	public void initMenu(SuperPanel p) {
		contentPane = p;
		setContentPane(contentPane);
		contentPane.revalidate();
		contentPane.repaint();
	}
	
	public GameSettings getGameSettings()
	{
		return gs;
	}
	
	public void setStandardGameSettings()
	{
		gs = new GameSettings(10, 150, 5);
	}
}
