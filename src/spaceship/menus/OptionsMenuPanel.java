package spaceship.menus;

import java.awt.Color;
import java.awt.event.KeyEvent;

import javax.swing.JPanel;

import spaceship.Main;
import spaceship.SuperPanel;

import javax.swing.JTextField;
import javax.swing.JLabel;
import javax.swing.SwingConstants;
import javax.swing.JButton;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.io.IOException;

import javax.swing.JSlider;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

public class OptionsMenuPanel extends SuperPanel {

	Main mf;
	private JTextField textFieldNumAsteroids;
	private JTextField textFieldNumEnemies;

	/**
	 * Create the panel.
	 */
	public OptionsMenuPanel(Main mf) {
		setBounds(mf.bounds());
		setBackground(new Color(0, 0, 0));
		setLayout(null);

		JSlider sliderAsteroids = new JSlider();
		sliderAsteroids.setValue(mf.getGameSettings().getNumAsteroids());
		sliderAsteroids.setMaximum(50);
		sliderAsteroids.setBounds(733, 305, 200, 26);
		sliderAsteroids.setBackground(new Color(0,0,0));
		add(sliderAsteroids);
		
		JSlider sliderEnemies = new JSlider();
		sliderEnemies.setValue(mf.getGameSettings().getNumEnemies());
		sliderEnemies.setMaximum(25);
		sliderEnemies.setBounds(733, 352, 200, 26);
		sliderEnemies.setBackground(new Color(0,0,0));
		add(sliderEnemies);
		
		textFieldNumAsteroids = new JTextField();
		textFieldNumAsteroids.setEditable(false);
		textFieldNumAsteroids.setHorizontalAlignment(SwingConstants.CENTER);
		textFieldNumAsteroids.setBackground(Color.BLACK);
		textFieldNumAsteroids.setForeground(Color.GREEN);
		textFieldNumAsteroids.setBounds(660, 301, 50, 30);
		add(textFieldNumAsteroids);
		textFieldNumAsteroids.setColumns(10);
		textFieldNumAsteroids.setText(""
				+ mf.getGameSettings().getNumAsteroids());

		JLabel lblAsteroids = new JLabel("Asteroids");
		lblAsteroids.setForeground(Color.RED);
		lblAsteroids.setBounds(576, 306, 69, 20);
		add(lblAsteroids);

		JButton btnSaveAndBack = new JButton("Save settings");
		btnSaveAndBack.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
			}
		});
		btnSaveAndBack.setBounds(438, 634, 200, 50);
		btnSaveAndBack.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {

				mf.initMenu(new MainMenuPanel(mf));

			}

		});
		add(btnSaveAndBack);

		JButton btnReset = new JButton("Reset");
		btnReset.setBounds(653, 634, 200, 50);
		btnReset.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				mf.setStandardGameSettings();
				mf.initMenu(new OptionsMenuPanel(mf));
			}

		});
		add(btnReset);

		textFieldNumEnemies = new JTextField();
		textFieldNumEnemies.setEditable(false);
		textFieldNumEnemies.setHorizontalAlignment(SwingConstants.CENTER);
		textFieldNumEnemies.setForeground(Color.GREEN);
		textFieldNumEnemies.setBackground(Color.BLACK);
		textFieldNumEnemies.setBounds(660, 352, 50, 30);
		textFieldNumEnemies.setText("" + mf.getGameSettings().getNumEnemies());
		add(textFieldNumEnemies);
		textFieldNumEnemies.setColumns(10);

		JLabel lblEnemies = new JLabel("Enemies");
		lblEnemies.setForeground(Color.RED);
		lblEnemies.setBounds(576, 357, 69, 20);
		add(lblEnemies);
		
		sliderAsteroids.addChangeListener(new ChangeListener(){

			@Override
			public void stateChanged(ChangeEvent e) {
				mf.getGameSettings().setNumAsteroids(sliderAsteroids.getValue());
				textFieldNumAsteroids.setText("" + mf.getGameSettings().getNumAsteroids());
			}
			
		});
		
		sliderEnemies.addChangeListener(new ChangeListener(){

			@Override
			public void stateChanged(ChangeEvent e) {
				mf.getGameSettings().setNumEnemies(sliderEnemies.getValue());
				textFieldNumEnemies.setText("" + mf.getGameSettings().getNumEnemies());
			}
			
		});
		
	}

	@Override
	public void keyPressedAction(KeyEvent e) {

	}

	@Override
	public void keyTypedAction(KeyEvent e) {

	}

	@Override
	public void keyReleasedAction(KeyEvent e) {

	}
}