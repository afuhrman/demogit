package spaceship.menus;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import javax.swing.JButton;

import spaceship.Main;
import spaceship.SuperPanel;
import spaceship.game.SpaceshipPanel;

@SuppressWarnings("serial")
public class MainMenuPanel extends SuperPanel {

	/**
	 * Create the panel.
	 */
	public MainMenuPanel(Main mf) {
		setBackground(Color.BLACK);
		setSize(1366, 768);
		setLayout(null);
		JButton btnStartGame = new JButton("Start Game");
		btnStartGame.setBounds(620, 198, 115, 30);
		btnStartGame.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				mf.initGame(new SpaceshipPanel(mf));
			}

		});
		add(btnStartGame);
		
		JButton btnOptions = new JButton("Options");
		btnOptions.setBounds(620, 279, 115, 30);
		btnOptions.addActionListener(new ActionListener(){

			@Override
			public void actionPerformed(ActionEvent e) {
				mf.initMenu(new OptionsMenuPanel(mf));
				
			}
			
		});
		add(btnOptions);

	}

	public void keyPressedAction(KeyEvent e) {
		;
	}

	@Override
	public void keyTypedAction(KeyEvent e) {
		;
	}

	@Override
	public void keyReleasedAction(KeyEvent e) {
		;
	}
}
