package spaceship;

import java.awt.event.KeyEvent;

import javax.swing.JPanel;

@SuppressWarnings("serial")
public abstract class SuperPanel extends JPanel {
	public SuperPanel() {
	}

	public abstract void keyPressedAction(KeyEvent e);
	public abstract void keyTypedAction(KeyEvent e);
	public abstract void keyReleasedAction(KeyEvent e);

}
